import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:project_sqlite/model/food_model.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_food.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE FoodDB (
          id INTEGER PRIMARY KEY,
          name TEXT,
          price TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<FoodModel>> getGroceries() async {
    Database db = await instance.database;
    var groceries = await db.query('FoodDB', orderBy: 'name');
    List<FoodModel> groceryList = groceries.isNotEmpty
        ? groceries.map((c) => FoodModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<FoodModel>> getProfile(int id) async {
    Database db = await instance.database;
    var profile = await db.query('FoodDB', where: 'id = ?', whereArgs: [id]);
    List<FoodModel> profileList = profile.isNotEmpty
        ? profile.map((c) => FoodModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(FoodModel grocery) async {
    Database db = await instance.database;
    return await db.insert('FoodDB', grocery.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('FoodDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(FoodModel grocery) async {
    Database db = await instance.database;
    return await db.update('FoodDB', grocery.toMap(),
        where: "id = ?", whereArgs: [grocery.id]);
  }
}
